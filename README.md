## 事前準備

### MongoDB起動
```
cd _docker/mongo
docker-compose up -d
```

### MongoDB Database作成
http://localhost:18081/ にアクセスしDB名`sample`作成



## サンプルAPI起動

### SpringBootAPI起動
```
cd spring-api/
./gradlew bootRun
```

### MicronautAPI起動
```
cd micronaut-api/
./gradlew run
```

### API (SpringBootAPI/MicronautAPI共通)
* http://localhost:8080/memo (POST) MongoDB登録
* http://localhost:8080/memo/{id(identity)} (GET) MongoDB参照
* http://localhost:8080/memo/dummy (GET) MongoDBアクセスなし

## ベンチマークツール使用例
wrkインストールはこちら
https://github.com/wg/wrk/wiki

### GETの場合
```
wrk -t5 -c10 -d30s http://localhost:8080/memo/dummy
```
### POSTの場合
```
wrk -t5 -c10 -d30s -s _wrk/post.lua http://localhost:8080/memo
```


