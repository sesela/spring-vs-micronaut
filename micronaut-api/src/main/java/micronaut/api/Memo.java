package micronaut.api;

import java.io.Serializable;
import java.util.UUID;

public class Memo implements Serializable {

	private String identity;
	private String title;
	private String memo;

	public Memo() {}

	public Memo(String title, String memo) {
		this.identity = UUID.randomUUID().toString();
		this.title = title;
		this.memo = memo;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Override
	public String toString() {
		return "Memo{" +
				"identity='" + identity + '\'' +
				", title='" + title + '\'' +
				", memo='" + memo + '\'' +
				'}';
	}
}