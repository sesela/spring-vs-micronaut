package micronaut.api;

import com.mongodb.client.model.Filters;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import javax.inject.Singleton;
import java.util.List;
import java.util.UUID;

@Singleton
public class MemoService {

	private final MongoClient mongoClient;
	private final MemoConfiguration memoConfiguration;

	public MemoService(MongoClient mongoClient, MemoConfiguration memoConfiguration) {
		this.mongoClient = mongoClient;
		this.memoConfiguration = memoConfiguration;
	}

	public Maybe<Memo> findById(String id) {
		return Flowable.fromPublisher(getCollection().find(Filters.eq("identity", id)).limit(1)).firstElement();
	}

	public Single<Memo> save(Memo memo) {
//		return findById(memo.getIdentity())
//				.switchIfEmpty(
//						Single.fromPublisher(getCollection().insertOne(memo))
//								.map(success -> memo)
//				);
		return Single.fromPublisher(getCollection().insertOne(memo)).map(success -> memo);
	}

	public Single<List<String>> getAll() {
		return Flowable.fromPublisher(getCollection().find()).map(Memo::getIdentity).toList();
	}

	private MongoCollection<Memo> getCollection() {
		return mongoClient
				.getDatabase(memoConfiguration.getDatabaseName())
				.getCollection(memoConfiguration.getCollectionName(), Memo.class);
	}
}