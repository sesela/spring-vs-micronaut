package micronaut.api;

import io.micronaut.context.annotation.ConfigurationProperties;

@ConfigurationProperties("memo")
public class MemoConfiguration {

	private String databaseName = "sample";
	private String collectionName = "mn-memo";

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
}
