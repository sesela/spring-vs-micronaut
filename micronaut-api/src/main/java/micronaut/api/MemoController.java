package micronaut.api;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Error;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.reactivex.Maybe;
import io.reactivex.Single;

import javax.inject.Singleton;
import java.util.List;

@Controller(value = "/memo")
@Secured({SecurityRule.IS_ANONYMOUS})
public class MemoController {

    private final MemoService memoService;

    public MemoController(MemoService memoService) {
        this.memoService = memoService;
    }

    @Get("/dummy")
    public Single<Memo> dummy() {
        return Single.just(new Memo());
    }

    @Post
    public Single<Memo> save(@QueryValue(value = "title", defaultValue = "non-title") String title, @QueryValue(value = "memo", defaultValue = "") String memo) {
       Single<Memo> singleMemo = memoService.save(new Memo(title, memo));
       return singleMemo;
    }

    @Get(value = "/{id}")
    public Maybe<Memo> findById(@PathVariable("id") String id) {
        Maybe<Memo> maybeMemo = memoService.findById(id);
        return maybeMemo;
    }

//    @Get(value = "/list")
//    public Single<List<String>> list() {
//        Single<List<String>> singleList = memoService.getAll();
//        return singleList;
//    }

    @Error(status = HttpStatus.NOT_FOUND, global = false)
    public HttpResponse notFound(HttpRequest request) {
        return HttpResponse.ok();
    }
}