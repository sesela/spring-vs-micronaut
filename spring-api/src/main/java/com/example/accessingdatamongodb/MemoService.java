package com.example.accessingdatamongodb;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class MemoService {

	private final MemoRepository repository;

	public MemoService(MemoRepository repository) {
		this.repository = repository;
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Memo save(Memo memo) {
		return repository.save(memo);
	}

	public Optional<Memo> findById(String id) {
		return repository.findById(id);
	}

	public List<String> getAllId() {
		return repository.findAll().stream().map(Memo::getId).collect(Collectors.toList());
	}

}
