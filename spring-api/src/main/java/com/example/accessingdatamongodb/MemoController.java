package com.example.accessingdatamongodb;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("memo")
public class MemoController {

	private final MemoService memoService;

	public MemoController(MemoService memoService) {
		this.memoService = memoService;
	}

	@GetMapping("/dummy")
	public Memo dummy() {
		return new Memo();
	}

	@PostMapping
	public Memo save(@RequestParam(value = "title", defaultValue = "non-title") String title, @RequestParam(value = "memo", defaultValue = "") String memo) {
		return memoService.save(new Memo(title, memo));
	}

	@GetMapping("/{id}")
	public Memo findById(@PathVariable("id") String id) {
		return memoService.findById(id).orElse(null);
	}

//	@GetMapping("/list")
//	public List<String> list() {
//		return memoService.getAllId();
//	}

}
