package com.example.accessingdatamongodb;

import org.springframework.data.annotation.Id;

import java.io.Serializable;


public class Memo implements Serializable {

    @Id
    private String id;
    private String title;
    private String memo;

    public Memo() {}

    public Memo(String title, String memo) {
        this.title = title;
        this.memo = memo;
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public String toString() {
        return "Memo{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", memo='" + memo + '\'' +
                '}';
    }
}

