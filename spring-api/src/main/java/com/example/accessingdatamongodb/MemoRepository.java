package com.example.accessingdatamongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MemoRepository extends MongoRepository<Memo, String> {

}
